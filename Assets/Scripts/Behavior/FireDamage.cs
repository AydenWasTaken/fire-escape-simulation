﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

/// <summary>
/// This script will get the position of the GameObject it is attached to, then check its own custom grid position.
/// If the custom grid position of the GameObject this is attached to is in a custom grid position with fire in it, take damage.
/// </summary>
public class FireDamage : MonoBehaviour
{
    [SerializeField] CGridScriptableObject gridObject;
    [SerializeField] IntScriptableObject globalSpeedMultiplierObject;
    [SerializeField] MapHandlerScriptableObject mapHandlerObject;
    [SerializeField] GameObjectListScriptableObject peopleList;

    [SerializeField] int maxHP = 3;
    int currentHP;

    [Tooltip("How much damage the GameObject takes when touching fire. Defaults to 1.")]
    [SerializeField] int damageToTake = 1;

    [Tooltip("The value of fire used in the grid. Used for detecting fire. Defaults to 2.")]
    [SerializeField] int fireBlockValue = 2;

    [Tooltip("The color the GameObject becomes after it dies. Defaults to red.")]
    [SerializeField] Color deathColor = Color.red;

    [Tooltip("How many seconds are between taking damage if continuously standing in fire. Defaults to 1.")]
    public float baseSecondsToWaitAfterTakingDamage = 1;
    float multipliedSecondsToWait;
    int currentMultiplier;
    float nextTakeDamageTime = 0;

    AILerp ai;

    bool canTakeDamage = true;

    private void Awake()
    {
        ai = GetComponent<AILerp>();
        currentHP = maxHP;
        currentMultiplier = globalSpeedMultiplierObject.Value;
    }

    private void Start()
    {
        //initial wait calculation
        multipliedSecondsToWait = baseSecondsToWaitAfterTakingDamage / globalSpeedMultiplierObject.Value;
    }

    private void Update()
    {
        //if the value changed
        if (globalSpeedMultiplierObject.Value != currentMultiplier)
        {
            //if the speed = 0 (paused)
            if (globalSpeedMultiplierObject.Value == 0)
            {
                canTakeDamage = false;
            }
            //if it isnt paused
            else
            {
                canTakeDamage = true;
                multipliedSecondsToWait = baseSecondsToWaitAfterTakingDamage / globalSpeedMultiplierObject.Value;
                currentMultiplier = globalSpeedMultiplierObject.Value;
            }
        }

        //if the GameObject this is attached to is in a custom grid position with fire
        if (gridObject.grid.GetValue(transform.position) == fireBlockValue || Input.GetKeyDown(KeyCode.P))
        {
            if (Time.time > nextTakeDamageTime)
            {
                TakeDamage();
                nextTakeDamageTime = Time.time + multipliedSecondsToWait;
            }
        }
    }

    /// <summary>
    /// Decreases HP by damageToTake.
    /// </summary>
    private void TakeDamage()
    {
        if (canTakeDamage)
        {
            currentHP -= damageToTake;
            Debug.Log($"{gameObject.name} took damage");

            if (currentHP == maxHP - 1)
            {
                mapHandlerObject.mapHandler.Injured++;
            }

            if (currentHP == 0)
            {
                mapHandlerObject.mapHandler.Injured--;
                Die();
            }
        }
    }

    /// <summary>
    /// Makes the GameObject this is attached to a different color and, if it has pathfinding, turns the pathfinding off.
    /// </summary>
    private void Die()
    {
        foreach (Renderer renderer in GetComponentsInChildren<Renderer>())
        {
            renderer.material.color = deathColor;
        }
        if (ai != null)
        {
            ai.canMove = false;
        }
        mapHandlerObject.mapHandler.Deaths++;
        Debug.Log($"{gameObject.name} died");
        peopleList.List.Remove(gameObject);
        this.enabled = false;
    }
}
