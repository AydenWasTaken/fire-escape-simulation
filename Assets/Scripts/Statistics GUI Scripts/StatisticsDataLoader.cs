﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class StatisticsDataLoader : MonoBehaviour
{
    [SerializeField] private MapHandlerScriptableObject mapHandlerObject;
    [SerializeField] private CGridScriptableObject gridObject;

    [SerializeField] private GameObject entryContainer;
    [SerializeField] private GameObject entryTemplate;

    private void Awake()
    {
        mapHandlerObject.mapHandler = new MapHandler(gridObject);

        entryTemplate.SetActive(false);

        //populate the list
        foreach (string fileName in Directory.GetFiles("./floorplans"))
        {
            string temp1 = fileName.Replace("./floorplans", "");
            string temp2 = temp1.Replace(".fsimdata", "");
            string trimmedFileName = temp2.Replace(@"\", "");

            LoadDataToMapHandlerScriptableObject(trimmedFileName);
            GameObject newEntry = Instantiate<GameObject>(entryTemplate, entryContainer.transform);

            SetEntryValues(newEntry, mapHandlerObject.mapHandler);

            newEntry.SetActive(true);

            Debug.Log($"Loaded floorplan with name: {trimmedFileName}");
        }
    }

    private void LoadDataToMapHandlerScriptableObject(string fileName)
    {
        mapHandlerObject.mapHandler.LoadStatistics(fileName);
    }

    private void SetEntryValues(GameObject entry, MapHandler mapHandler)
    {
        entry.transform.Find("FloorplanNameImage").GetComponentInChildren<Text>().text = mapHandler.CurrentMapName;
        entry.transform.Find("AvgSimulationTimeTitleImage").GetComponentInChildren<Text>().text = mapHandler.mapStatistics.AverageSimulationTime.ToString();
        entry.transform.Find("AvgInjuriesTitleImage").GetComponentInChildren<Text>().text = mapHandler.mapStatistics.AverageOfInjured.ToString();
        entry.transform.Find("AvgDeathsTitleImage").GetComponentInChildren<Text>().text = mapHandler.mapStatistics.AverageOfDeaths.ToString();
        entry.transform.Find("AvgBurntTilesTitleImage").GetComponentInChildren<Text>().text = mapHandler.mapStatistics.AverageOfBurntTiles.ToString();
        entry.transform.Find("AvgTotalPeopleTitleImage").GetComponentInChildren<Text>().text = mapHandler.mapStatistics.AveragenrOfPeople.ToString();
    }
}