﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    [SerializeField] GameObject tutorialscreen;

    public void ToggleTutorialScreen()
    {
        tutorialscreen.SetActive(!tutorialscreen.activeSelf);
    }
}
