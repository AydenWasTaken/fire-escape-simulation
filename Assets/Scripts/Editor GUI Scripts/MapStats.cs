﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is where the average of all runs of the simulation are stored for the current floorplan
/// </summary>
[System.Serializable]
public class MapStats
{
    //this will be used to store the results of the simulations
    public int NumberOfRuns;
    public double AveragenrOfPeople;
    public double AverageOfInjured;
    public double AverageOfDeaths;
    public double AverageOfBurntTiles;
    public double AverageSimulationTime;
}
