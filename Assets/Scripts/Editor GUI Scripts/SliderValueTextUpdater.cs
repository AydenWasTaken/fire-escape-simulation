﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderValueTextUpdater : MonoBehaviour
{
    [SerializeField] Slider slider;
    [SerializeField] Text text;

    public void UpdateText()
    {
        text.text = slider.value.ToString();
    }
}
