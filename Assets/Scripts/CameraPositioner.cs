﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPositioner : MonoBehaviour
{
    [SerializeField] CGridScriptableObject gridObject;
    void Start()
    {
         transform.position = new Vector3((gridObject.grid.Width / 2) + gridObject.grid.OriginPosition.x, (gridObject.grid.Height / 2) + gridObject.grid.OriginPosition.y, -((gridObject.grid.Width /2) + (gridObject.grid.Height /2)));
    }

}
