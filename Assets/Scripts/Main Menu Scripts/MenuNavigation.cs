﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuNavigation : MonoBehaviour
{
  public void FloorplanEditor()
    {
        SceneManager.LoadScene("FinalBuildScene");
    }

    public void Statistics()
    {
        SceneManager.LoadScene("Statistics");
    }

    public void Exit()
    {
        Debug.Log("Exit");
        Application.Quit();
    }
}
