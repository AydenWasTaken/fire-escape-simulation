﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class ScriptableObjectsInitializers : MonoBehaviour
{
    [Header("Grid")]
    [SerializeField] CGridScriptableObject gridObject;
    [SerializeField] int gridWidth = 20;
    [SerializeField] int gridHeight = 10;
    [SerializeField] float cellSize = 1f;
    [SerializeField] GameObject groundPrefab;
    [SerializeField] GameObject wall;

    [Header("Map Handler")]
    [SerializeField] MapHandlerScriptableObject mapHandlerObject;

    [Header("Global Speed Modifier")]
    [SerializeField] IntScriptableObject speedMultiplierObject;

    [Header("People List")]
    [SerializeField] GameObjectListScriptableObject peopleList;

    void Awake()
    {
        gridObject.grid = new CGrid(gridWidth, gridHeight, cellSize, groundPrefab, new Vector3(-10, -5), wall); //temp origin point until vlad fixed clicking
        mapHandlerObject.mapHandler = new MapHandler(gridObject);
        speedMultiplierObject.Value = 1;
        peopleList.List = new List<GameObject>();
    }
}
