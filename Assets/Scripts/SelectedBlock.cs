﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectedBlock : MonoBehaviour
{
    [SerializeField] GameObject buildingblock;
    [SerializeField] private int value;
    [SerializeField] BuildingController BController;
    [SerializeField] CGridScriptableObject MainGrid;
    GameObject selectedblock;

    public void SelectBlock()
    {
        selectedblock = GameObject.FindGameObjectWithTag("SelectedBlock");
        buildingblock.transform.localScale = new Vector2(MainGrid.grid.CellSize, MainGrid.grid.CellSize);
        SelectedBlockVariable.BuildingBlock = buildingblock;
        SelectedBlockVariable.Value = value;
        SpriteRenderer sblock = selectedblock.GetComponent<SpriteRenderer>();
        SpriteRenderer bblock = buildingblock.GetComponent<SpriteRenderer>();
        sblock.sprite = bblock.sprite;
        sblock.sortingOrder = 10;
        sblock.transform.localScale = buildingblock.transform.localScale;
        sblock.color = new Color(1f, 1f, 1f, .5f);
        BController.SelectedBlock(selectedblock);
    }
    public void SelectBlock(bool notselected)
    {
        Debug.Log("FIRE");
        selectedblock = GameObject.FindGameObjectWithTag("SelectedBlock");
        buildingblock.transform.localScale = new Vector2(MainGrid.grid.CellSize, MainGrid.grid.CellSize);
        SelectedBlockVariable.BuildingBlock = buildingblock;
        SelectedBlockVariable.Value = value;
        SpriteRenderer sblock = selectedblock.GetComponent<SpriteRenderer>();
        SpriteRenderer bblock = buildingblock.GetComponent<SpriteRenderer>();
        sblock.sprite = bblock.sprite;
        sblock.sortingOrder = 10;
        sblock.transform.localScale = buildingblock.transform.localScale;
        sblock.color = new Color(1f, 1f, 1f, 0f);
        BController.SelectedBlock(selectedblock);
    }
}
